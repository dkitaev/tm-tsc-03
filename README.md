# TASK MANAGER

SCREENSHOTS

https://drive.google.com/drive/folders/1NVrzH6noO6bkUO_uMTGHWh7D0bAFbq5A?usp=sharing

## DEVELOPER INFO

name: Danil Kitaev

e-mail: danil.kitaev1996@gmail.com

## HARDWARE

CPU: i5

RAM: 6G

SSD: 90GB

## SOFTWARE

System: Windows 10 Version 1909

Version JDK: 1.8.0_282

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
